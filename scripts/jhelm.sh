#!/usr/bin/env bash
set -eu

[[ ${DEBUG:=false} ]]

$DEBUG && set -x

[[ $name ]]
[[ $namespace ]]
[[ $workspace ]]

export HELM_NAMESPACE=${namespace}

update() {
  [[ $source ]]
  [[ $branch ]]
  [[ $chart ]]
  [[ $gitsha ]]
  [[ _$extra_files ]]
  [[ $checksum_file ]]
  [[ $helm_status_file ]]
  [[ $helm_history_file ]]

  source=$(realpath "$source")
  checksum_file=$(realpath "$checksum_file")
  helm_status_file=$(realpath "$helm_status_file")
  helm_history_file=$(realpath "$helm_history_file")

  cd "$workspace"

  export GIT_DIR="$source"

  # extract
  git archive ${gitsha} | tar x ${chart} ${extra_files}

  # save list
  git archive ${gitsha} | tar t ${chart} ${extra_files} | sort > .files

  # hashes
  tar c -T .files --mtime=1970-01-01 | sha1sum | awk '{print $1}' > "$checksum_file"

#  # package helm chart
#  local tmpdir
#  tmpdir=$(mktemp -d) && trap "rm -rf $tmpdir" EXIT
#  helm package ${chart} -d "$tmpdir" --version= --app-version=
#  mv -f $tmpdir/*tgz chart.tgz

  # helm status
  helm status ${name} --show-desc -o json > ${helm_status_file} 2>/dev/null || true
  helm history ${name} -o json > ${helm_history_file} 2>/dev/null || true

}

upgrade() {
  [[ $name ]]
  [[ $namespace ]]
  [[ $workspace ]]
  [[ $chart ]]
  [[ $helm_secrets_key ]]
  [[ $value_files ]]
  [[ $app_version ]]
  # output files
  [[ $helm_status_file ]]
  [[ $helm_history_file ]]

  helm_status_file=$(realpath "$helm_status_file")
  helm_history_file=$(realpath "$helm_history_file")

  cd ${workspace}

  # package helm chart
  local fchart="chart.tgz"
  local tmpdir
  tmpdir=$(mktemp -d -p .) && trap "rm -rf $tmpdir" EXIT
  helm package ${chart} --app-version="$app_version" -d "$tmpdir"
  mv -f "$tmpdir"/*.tgz "$fchart"

  local fsecrets="secrets.key"
  install -m600 /dev/stdin ${fsecrets} <<<"$helm_secrets_key"
  #export HELM_SECRETS_KEY="$fsecrets"

  local opts=''
  IFS=',' read -ra fargs <<<"$value_files"
  for i in ${fargs[@]}; do opts+="-f $i "; done

  opts+=" --set global.chart.gitsha=$gitsha"
  opts+=" --set global.chart.checksum=$checksum"

  helm upgrade --install ${name} "$fchart" ${opts} --debug && ok=true || ok=false

  helm status ${name} --show-desc -o json > ${helm_status_file}
  helm history ${name} -o json > ${helm_history_file}

  $ok || return 1
}

uninstall() {
  if helm uninstall ${name}; then
    return 0 # uninstalled
  fi
  if ! helm status ${name}; then
    return 0 # missing, noop, already uninstalled?
  fi
  echo "Failed to uninstall $name, status unknown"
  return 1
}

"$@"
