#!/usr/bin/env bash
set -eu

: ${DEBUG:=false}
$DEBUG && set -x

[[ $name ]]
[[ $namespace ]]
[[ $generation ]]
[[ $workspace ]]

configure() {
  [[ $ssh_private_key ]]
  [[ $ssh_known_hosts ]]

  : ${f_generation:=".generation"}
  : ${f_ssh_key:="id_rsa"}
  : ${f_ssh_known_hosts:="known_hosts"}

  # force reinit if spec changed
  if [[ -f "$workspace/$f_generation" && "$generation" != "$(cat "$workspace/$f_generation")" ]]; then
    rm -rf "$workspace"
  fi

  mkdir -p "$workspace"
  cd "$workspace"

  if [[ ! -f config ]]; then
    git init --bare 2>/dev/null
    git config core.sshCommand "ssh -F /dev/null -i $f_ssh_key -o UserKnownHostsFile=$f_ssh_known_hosts"
    git remote add origin "$url"
  else
    git remote set-url origin "$url"
  fi

  install -m 600 /dev/stdin "$f_ssh_key" <<<"$ssh_private_key"
  install -m 600 /dev/stdin "$f_ssh_known_hosts" <<<"$ssh_known_hosts"

  echo -n "$generation" > "$f_generation"
}

update() {
  configure
  git fetch --no-tags --prune
}

delete() {
  rm -rf "$workspace"
}

"$@"
