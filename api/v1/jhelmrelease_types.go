/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

// JHelmRelease is the Schema for the jhelms API
//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:path=jhelms,singular=jhelm,shortName=jh;jhr
//+kubebuilder:printcolumn:name=Status,type=string,JSONPath=".status.status"
//+kubebuilder:printcolumn:name=Reason,type=string,JSONPath=".status.reason"
//+kubebuilder:printcolumn:name=Repo,type=string,JSONPath=".spec.source.ref",priority=1
//+kubebuilder:printcolumn:name=Branch,type=string,JSONPath=".spec.source.branch",priority=1
//+kubebuilder:printcolumn:name=Chart,type=string,JSONPath=".spec.source.chart"
//+kubebuilder:printcolumn:name=Version,type=string,JSONPath=".status.chartVersion"
//+kubebuilder:printcolumn:name=AppVersion,type=string,JSONPath=".status.appVersion"
//+kubebuilder:printcolumn:name=Revision,type=string,JSONPath=".status.revision"
//+kubebuilder:printcolumn:name=Updated,type=date,JSONPath=".status.updated"
//+kubebuilder:printcolumn:name=Age,type=date,JSONPath=".metadata.creationTimestamp"
type JHelmRelease struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   JHelmReleaseSpec   `json:"spec,omitempty"`
	Status JHelmReleaseStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// JHelmReleaseList contains a list of JHelmRelease
type JHelmReleaseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []JHelmRelease `json:"items"`
}

// JHelmReleaseSpec defines the desired state of JHelmRelease
type JHelmReleaseSpec struct {
	Source     Source   `json:"source"`
	Secrets    Secrets  `json:"secrets,omitempty"`
	ExtraFiles []string `json:"extraFiles,omitempty"`
	ValueFiles []string `json:"valueFiles,omitempty"`
	Package    Package  `json:"package,omitempty"`
	//+kubebuilder:default="5m"
	UpdateInterval metav1.Duration `json:"updateInterval,omitempty"`
}

type Source struct {
	Ref    string `json:"ref"`
	Branch string `json:"branch"`
	Chart  string `json:"chart"`
}

type Secrets struct {
	Ref string `json:"ref"`
	Key string `json:"key"`
}

type Package struct {
	//+kubebuilder:default=false
	SetAppVersion bool   `json:"setAppVersion,omitempty"`
	FromFile      string `json:"fromFile,omitempty"`
	ValuePath     string `json:"valuePath,omitempty"`
}

// JHelmReleaseStatus defines the observed state of JHelmRelease
type JHelmReleaseStatus struct {
	Status         string                        `json:"status"`
	Reason         string                        `json:"reason"`
	SpecSha        string                        `json:"specSha,omitempty"`
	GitSha         string                        `json:"gitSha,omitempty"`
	Checksum       string                        `json:"checksum,omitempty"`
	ChartVersion   string                        `json:"chartVersion,omitempty"`
	AppVersion     string                        `json:"appVersion,omitempty"`
	Revision       int64                         `json:"revision,omitempty"`
	Updated        string                        `json:"updated,omitempty"`
	Description    string                        `json:"description,omitempty"`
	LastReconciled metav1.Time                   `json:"lastReconciled,omitempty"`
	Conditions     []JHelmReleaseStatusCondition `json:"conditions,omitempty"`
}

type JHelmReleaseStatusCondition struct {
	metav1.Condition `json:",inline"`
}

//type JHelmWorkspaceState struct {
//  Generation int64
//  Branch string
//  GitSha string
//  Checksum string
//}

func init() {
	SchemeBuilder.Register(&JHelmRelease{}, &JHelmReleaseList{})
}

///

func (r *JHelmRelease) GetReconciliationRequested() *time.Time {
	if x := r.Annotations["reconciliation-requested"]; x != "" {
		if parsed, err := time.Parse(time.RFC3339, x); err == nil {
			return &parsed
		}
	}
	return nil
}

/// conditions

func (r *JHelmRelease) FindStatusCondition(conditionType string) *JHelmReleaseStatusCondition {
	s := &r.Status
	for i := range s.Conditions {
		c := &s.Conditions[i]
		if c.Type == conditionType {
			return c
		}
	}
	return nil
}

func (r *JHelmRelease) SetStatusCondition(newCondition JHelmReleaseStatusCondition) *JHelmReleaseStatusCondition {
	s := &r.Status
	if newCondition.LastTransitionTime.IsZero() {
		newCondition.LastTransitionTime = metav1.NewTime(time.Now())
	}
	c := r.FindStatusCondition(newCondition.Type)
	if c != nil {
		newCondition.DeepCopyInto(c)
		return c
	} else {
		s.Conditions = append(s.Conditions, newCondition)
		return &newCondition
	}
}

func (r *JHelmRelease) Ready(reason string) {
	c := &JHelmReleaseStatusCondition{}
	c.Type = "Ready"
	c.Status = metav1.ConditionTrue
	c.Reason = reason
	c.ObservedGeneration = r.Generation
	c = r.SetStatusCondition(*c)
	r.Status.Status = "Ready"
	r.Status.Reason = reason
	r.Status.LastReconciled = c.LastTransitionTime
}

func (r *JHelmRelease) NotReady(reason string, message string) {
	c := JHelmReleaseStatusCondition{}
	c.Type = "Ready"
	c.Status = metav1.ConditionFalse
	c.Reason = reason
	c.Message = message
	r.Status.Status = "NotReady"
	r.Status.Reason = reason
	r.SetStatusCondition(c)
}

func (r *JHelmRelease) IsReady() bool {
	return r.Status.Status == "Ready"
}
