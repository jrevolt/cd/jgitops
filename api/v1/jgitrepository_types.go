/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

// JGitRepository is the Schema for the jgits API
//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:path=jgits,singular=jgit,shortName=jg;jgr
//+kubebuilder:printcolumn:name=Status,type=string,JSONPath=".status.status"
//+kubebuilder:printcolumn:name=Reason,type=string,JSONPath=".status.reason"
//+kubebuilder:printcolumn:name=URL,type=string,JSONPath=".spec.url"
//+kubebuilder:printcolumn:name=Updated,type=date,JSONPath=".status.lastReconciled"
//+kubebuilder:printcolumn:name=Age,type=date,JSONPath=".metadata.creationTimestamp"
type JGitRepository struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   JGitRepositorySpec   `json:"spec"`
	Status JGitRepositoryStatus `json:"status,omitempty"`
}

// JGitRepositoryList contains a list of JGitRepository
//+kubebuilder:object:root=true
type JGitRepositoryList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []JGitRepository `json:"items"`
}

// JGitRepositorySpec defines the desired state of JGitRepository
type JGitRepositorySpec struct {
	Url    string `json:"url"`
	Secret Secret `json:"secret,omitempty"`
	//+kubebuilder:default="1m"
	UpdateInterval metav1.Duration `json:"updateInterval,omitempty"`
}

type Secret struct {
	Ref string `json:"ref"`
	// SSH private key as expected by SSH client
	SshPrivateKey string `json:"sshPrivateKey"`
	// SSH's known_hosts for SSH client
	SshKnownHosts string `json:"sshKnownHosts"`
}

// JGitRepositoryStatus defines the observed state of JGitRepository
type JGitRepositoryStatus struct {
	Status         string                          `json:"status,omitempty"`
	Reason         string                          `json:"reason,omitempty"`
	SpecSha        string                          `json:"specSha,omitempty"`
	Revisions      map[string]string               `json:"revisions,omitempty"`
	LastReconciled metav1.Time                     `json:"lastReconciled,omitempty"`
	Conditions     []JGitRepositoryStatusCondition `json:"conditions,omitempty"`
}

type JGitRepositoryStatusCondition struct {
	metav1.Condition `json:",inline"`
}

func init() {
	SchemeBuilder.Register(&JGitRepository{}, &JGitRepositoryList{})
}

///

func (r *JGitRepository) GetReconciliationRequested() *time.Time {
	if x := r.Annotations["reconciliation-requested"]; x != "" {
		if parsed, err := time.Parse(time.RFC3339, x); err == nil {
			return &parsed
		}
	}
	return nil
}

func (r *JGitRepository) FindStatusCondition(conditionType string) int {
	s := r.Status
	for i := range s.Conditions {
		c := s.Conditions[i]
		if c.Type == conditionType {
			return i
		}
	}
	return -1
}

func (r *JGitRepository) SetStatusCondition(newCondition JGitRepositoryStatusCondition) *JGitRepositoryStatusCondition {
	s := &r.Status
	if newCondition.LastTransitionTime.IsZero() {
		newCondition.LastTransitionTime = metav1.NewTime(time.Now())
	}
	if newCondition.ObservedGeneration == 0 {
		newCondition.ObservedGeneration = r.Generation
	}
	c := r.FindStatusCondition(newCondition.Type)
	if c != -1 {
		newCondition.DeepCopyInto(&(s.Conditions[c]))
		return &s.Conditions[c]
	} else {
		s.Conditions = append(s.Conditions, newCondition)
		return &newCondition
	}
}

func (r *JGitRepository) Ready(reason string) {
	c := &JGitRepositoryStatusCondition{}
	c.Type = "Ready"
	c.Status = metav1.ConditionTrue
	c.Reason = reason
	c = r.SetStatusCondition(*c)
	r.Status.Status = "Ready"
	r.Status.Reason = reason
	r.Status.LastReconciled = c.LastTransitionTime
}

func (r *JGitRepository) NotReady(reason string, message string) {
	r.Status.Status = "NotReady"
	r.Status.Reason = reason
	c := JGitRepositoryStatusCondition{}
	c.Type = "Ready"
	c.Status = metav1.ConditionFalse
	c.Reason = reason
	c.Message = message
	r.SetStatusCondition(c)
}

func (r *JGitRepository) IsReady() bool {
	return r.Status.Status == "Ready"
}
