package api

type JGitopsOperatorOptions struct {
	Namespace string
	Workspace string
	Bash      string
	Scripts   string
	Plugins   string
}
