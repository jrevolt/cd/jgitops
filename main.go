/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"git.jrevolt.io/jrevolt/cd/jgitops/api"
	"go.uber.org/zap/zapcore"
	"os"
	"path/filepath"
	"strings"
	"time"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	cdv1 "git.jrevolt.io/jrevolt/cd/jgitops/api/v1"
	"git.jrevolt.io/jrevolt/cd/jgitops/controllers"
	//+kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(cdv1.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

const (
	envJGitopsNamespace = "JGITOPS_NAMESPACE"
	envJGitopsWorkspace = "JGITOPS_WORKSPACE"
	envJGitopsScripts   = "JGITOPS_SCRIPTS"
	envJGitopsPlugins   = "JGITOPS_PLUGINS"
)

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	opts := zap.Options{
		Development:     false,
		Level:           zapcore.InfoLevel,
		StacktraceLevel: zapcore.PanicLevel,
	}
	opts.BindFlags(flag.CommandLine)

	InitEnv(envJGitopsNamespace, "")
	InitEnv(envJGitopsWorkspace, ".workspace")
	InitEnv(envJGitopsScripts, "scripts")
	InitEnv(envJGitopsPlugins, "helm-plugins")
	var jopts api.JGitopsOperatorOptions
	flag.StringVar(&jopts.Namespace, "namespace", "", "k8s namespace")
	flag.StringVar(&jopts.Workspace, "workspace", os.Getenv(envJGitopsWorkspace), "path to jgitops workspace")
	flag.StringVar(&jopts.Bash, "bash", "bash", "path to bash executable")
	flag.StringVar(&jopts.Scripts, "scripts", os.Getenv(envJGitopsScripts), "path to jgitops shell scripts")
	flag.StringVar(&jopts.Plugins, "plugins", os.Getenv(envJGitopsPlugins), "path to jgitops/helm plugins")

	flag.Parse()

	if jopts.Namespace == "" {
		if f, err := os.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/namespace"); err == nil {
			jopts.Namespace = strings.TrimSpace(string(f))
		}
	}
	if jopts.Namespace == "" {
		panic("namespace?")
	}

	if jopts.Plugins != "" {
		k := "HELM_PLUGINS"
		v := GetAbsPath(jopts.Plugins)
		if x := os.Getenv(k); x != "" {
			v = v + string(os.PathListSeparator) + x
		}
		if err := os.Setenv(k, v); err != nil {
			panic(err)
		}
	}

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	syncPeriod, _ := time.ParseDuration("1h")

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                  scheme,
		MetricsBindAddress:      metricsAddr,
		Port:                    9443,
		HealthProbeBindAddress:  probeAddr,
		LeaderElection:          enableLeaderElection,
		LeaderElectionID:        "cd.jrevolt.io",
		LeaderElectionNamespace: jopts.Namespace,
		Namespace:               jopts.Namespace,
		SyncPeriod:              &syncPeriod,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.JGitRepositoryReconciler{
		Client:  mgr.GetClient(),
		Scheme:  mgr.GetScheme(),
		Options: jopts,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "JGitRepository")
		os.Exit(1)
	}
	if err = (&controllers.JHelmReleaseReconciler{
		Client:  mgr.GetClient(),
		Scheme:  mgr.GetScheme(),
		Options: jopts,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "JHelmRelease")
		os.Exit(1)
	}
	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}

func InitEnv(name string, dflt string) string {
	v := os.Getenv(name)
	if v == "" {
		v = dflt
		if err := os.Setenv(name, v); err != nil {
			panic(err)
		}
	}
	return v
}

func GetAbsPath(path string) string {
	if x, err := filepath.Abs(path); err == nil {
		return x
	} else {
		return path
	}
}
