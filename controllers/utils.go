package controllers

import (
	"crypto/sha1"
	"fmt"
	"k8s.io/apimachinery/pkg/util/json"
	"os"
	"os/exec"
	"strconv"
)

func LoadFile(fname string) *string {
	if data, err := os.ReadFile(fname); err == nil {
		result := string(data)
		return &result
	}
	return nil
}

func LoadFileInt64(fname string) *int64 {
	if x := LoadFile(fname); x != nil {
		if i, err := strconv.ParseInt(*x, 10, 64); err == nil {
			return &i
		}
	}
	return nil
}

func SaveFile(fname string, data string) {
	if f, err := os.Create(fname); err == nil {
		f.WriteString(data)
		defer f.Close()
	}
}

func LoadObject(fname string, dst interface{}) {
	if data, err := os.ReadFile(fname); err == nil {
		if err := json.Unmarshal(data, &dst); err != nil {
			return
		}
	}
}

func SaveObject(fname string, src interface{}) {
	if data, err := json.Marshal(src); err == nil {
		if f, err := os.Create(fname); err == nil {
			f.Write(data)
			defer f.Close()
		}
	}
}

func GetShaSum(data ...interface{}) string {
	sha := sha1.New()
	bin := sha.Sum([]byte(fmt.Sprintf("%v", data)))
	x := fmt.Sprintf("%x", bin)[:40]
	return x
}

func GetExitErrorStdErr(err *error) string {
	if exiterr, ok := (*err).(*exec.ExitError); ok {
		return string(exiterr.Stderr)
	} else {
		return (*err).Error()
	}
}
