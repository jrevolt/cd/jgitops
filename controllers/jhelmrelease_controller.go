/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"git.jrevolt.io/jrevolt/cd/jgitops/api"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"os/exec"
	"path"
	"reflect"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
	"strings"
	"time"

	cdv1 "git.jrevolt.io/jrevolt/cd/jgitops/api/v1"
	v1 "k8s.io/api/core/v1"
)

// JHelmReleaseReconciler reconciles a JHelmRelease object
type JHelmReleaseReconciler struct {
	client.Client
	Scheme  *runtime.Scheme
	Options api.JGitopsOperatorOptions
}

//+kubebuilder:rbac:groups=cd.jrevolt.io,resources=jhelms,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=cd.jrevolt.io,resources=jhelms/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=cd.jrevolt.io,resources=jhelms/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the JHelmRelease object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.10.0/pkg/reconcile
func (r *JHelmReleaseReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.FromContext(ctx)

	jhelm := &cdv1.JHelmRelease{}
	if err := r.Get(ctx, req.NamespacedName, jhelm); err != nil {
		if errors.IsNotFound(err) {
			log.Info("Deleted") // assuming all cleanup done in finalizer
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	log.V(1).Info(fmt.Sprintf("generation=%d, version=%s", jhelm.Generation, jhelm.ResourceVersion))

	jgit := &cdv1.JGitRepository{}
	if err := r.Get(ctx, r.ObjectName(jhelm.Spec.Source.Ref), jgit); err != nil {
		if errors.IsNotFound(err) {
			jhelm.NotReady("SourceNotFound", fmt.Sprintf("source not found: %s", jhelm.Spec.Source.Ref))
			r.UpdateStatus(ctx, jhelm)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if !jgit.IsReady() {
		s := &jgit.Status
		jhelm.NotReady("SourceNotReady", fmt.Sprintf("source not ready: source=%s, status=%s, reason=%s", jgit.Name, s.Status, s.Reason))
		r.UpdateStatus(ctx, jhelm)
		return ctrl.Result{RequeueAfter: 15 * time.Second}, nil
	}

	secret := &v1.Secret{}
	if err := r.Get(ctx, r.ObjectName(jhelm.Spec.Secrets.Ref), secret); err != nil {
		if errors.IsNotFound(err) {
			jhelm.NotReady("Invalid", err.Error())
			r.UpdateStatus(ctx, jhelm)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	isReconciliationRequested := false
	if x := jhelm.GetReconciliationRequested(); x != nil {
		isReconciliationRequested = x.After(jhelm.Status.LastReconciled.Time)
	}

	specShaSum := GetShaSum(jhelm.Spec, jgit.Spec, secret.Data)
	outdated := specShaSum != jhelm.Status.SpecSha

	log = log.WithValues("generation", jhelm.Generation, "resourceVersion", jhelm.ResourceVersion)

	// register finalizer if needed
	finalizer := "jgitops"
	if !controllerutil.ContainsFinalizer(jhelm, finalizer) {
		controllerutil.AddFinalizer(jhelm, finalizer)
		if err := r.Update(ctx, jhelm); err != nil {
			return ctrl.Result{}, nil
		}
	}

	workspace := path.Join(r.Options.Workspace, "jhelm", jhelm.Name)
	script := path.Join(r.Options.Scripts, "jhelm.sh")

	// finalization
	if jhelm.GetDeletionTimestamp() != nil && controllerutil.ContainsFinalizer(jhelm, finalizer) {
		log.Info("Uninstalling")
		jhelm.NotReady("Uninstaling", "Uninstalling helm release")
		r.UpdateStatus(ctx, jhelm)

		if err := os.RemoveAll(workspace); err != nil {
			log.Error(err, fmt.Sprintf("Failed to cleanup workspace %s", workspace))
		}

		cmdctx, cancel := context.WithTimeout(ctx, 1*time.Minute)
		defer cancel()
		cmd := exec.CommandContext(cmdctx, r.Options.Bash, script, "uninstall")
		cmd.Env = append(os.Environ(),
			fmt.Sprintf("name=%s", jhelm.Name),
			fmt.Sprintf("namespace=%s", jhelm.Namespace),
			fmt.Sprintf("workspace=%s", workspace),
		)
		if _, err := cmd.Output(); err != nil {
			jhelm.NotReady("UninstallFailed", GetExitErrorStdErr(&err))
			r.UpdateStatus(ctx, jhelm)
			return ctrl.Result{RequeueAfter: 15 * time.Second}, nil
		}

		controllerutil.RemoveFinalizer(jhelm, finalizer)
		if err := r.Update(ctx, jhelm); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, nil
	}

	os.MkdirAll(workspace, os.ModeDir)

	//state := &cdv1.JHelmWorkspaceState{}
	//fState := path.Join(workspace, ".state.json")
	//LoadObject(fState, state)

	fSpecSha := path.Join(workspace, ".specsha")
	specShaSumLoaded := LoadFile(fSpecSha)
	outdated = outdated || specShaSumLoaded == nil || *specShaSumLoaded != specShaSum

	fGitSha := path.Join(workspace, ".gitsha")
	gitsha := LoadFile(fGitSha)
	outdated = outdated || gitsha == nil

	fChecksum := path.Join(workspace, ".checksum")
	checksum := LoadFile(fChecksum)
	outdated = outdated || checksum == nil

	fHelmStatus := path.Join(workspace, ".helm-status")
	helmStatus := HelmStatus{}
	LoadObject(fHelmStatus, &helmStatus)
	outdated = outdated || helmStatus.Info.Status != "deployed"

	fHelmHistory := path.Join(workspace, ".helm-history")
	helmRevision := LoadHelmRevision(fHelmHistory)
	outdated = outdated || helmRevision == nil || helmRevision.Revision != jhelm.Status.Revision

	sourceWorkspace := path.Join(r.Options.Workspace, "jgit", jgit.Name)
	sourceBranch := jhelm.Spec.Source.Branch
	sourceGitSha := jgit.Status.Revisions[sourceBranch]
	//outdated = outdated || jhelm.Status.GitSha != sourceGitSha

	valueFiles := strings.Join(jhelm.Spec.ValueFiles, ",")
	extraFiles := strings.Join(jhelm.Spec.ExtraFiles, ",")
	helmSecretsKey := secret.Data[jhelm.Spec.Secrets.Key]

	// todo update
	cmdctx, cancel := context.WithTimeout(ctx, 60*time.Second)
	defer cancel()
	cmd := exec.CommandContext(cmdctx, r.Options.Bash, script, "update")
	cmd.Env = append(os.Environ(),
		fmt.Sprintf("name=%s", jhelm.Name),
		fmt.Sprintf("namespace=%s", jhelm.Namespace),
		fmt.Sprintf("workspace=%s", workspace),
		fmt.Sprintf("source=%s", sourceWorkspace),
		fmt.Sprintf("branch=%s", jhelm.Spec.Source.Branch),
		fmt.Sprintf("chart=%s", jhelm.Spec.Source.Chart),
		fmt.Sprintf("gitsha=%s", sourceGitSha),
		fmt.Sprintf("value_files=%s", valueFiles),
		fmt.Sprintf("extra_files=%s", extraFiles),
		fmt.Sprintf("helm_secrets_key=%s", helmSecretsKey),
		fmt.Sprintf("checksum_file=%s", fChecksum),
		fmt.Sprintf("helm_status_file=%s", fHelmStatus),
		fmt.Sprintf("helm_history_file=%s", fHelmHistory),
	)
	if _, err := cmd.Output(); err != nil {
		jhelm.NotReady("WorkspaceUpdateFailed", GetExitErrorStdErr(&err))
		r.UpdateStatus(ctx, jhelm)
		return r.Result(jhelm)
	}

	checksum = LoadFile(fChecksum)
	outdated = outdated || checksum == nil || *checksum != jhelm.Status.Checksum

	helmRevision = LoadHelmRevision(fHelmHistory)
	outdated = outdated || helmRevision == nil || helmRevision.Revision != jhelm.Status.Revision || helmRevision.Status != "deployed"

	if jhelm.IsReady() && !outdated {
		status := jhelm.Status.DeepCopy()
		if isReconciliationRequested {
			//status.LastReconciled = metav1.NewTime(time.Now())
		}
		if status.GitSha != sourceGitSha {
			status.GitSha = sourceGitSha
			SaveFile(fGitSha, status.GitSha)
		}
		if !reflect.DeepEqual(*status, jhelm.Status) {
			jhelm.Status = *status
			r.UpdateStatus(ctx, jhelm)
		}
		return r.Result(jhelm)
	}

	var appVersion string
	if x, err := r.LoadAppVersion(jhelm); err == nil {
		appVersion = *x
	} else {
		jhelm.NotReady("AppVersionError", err.Error())
		r.UpdateStatus(ctx, jhelm)
		return r.Result(jhelm)
	}

	jhelm.NotReady("UpgradeInProgress", fmt.Sprintf("gitsha=%s, checksum=%s", sourceGitSha, checksum))
	r.UpdateStatus(ctx, jhelm)

	cmdctx, cancel = context.WithTimeout(ctx, 5*time.Minute)
	defer cancel()
	cmd = exec.CommandContext(cmdctx, r.Options.Bash, script, "upgrade")
	cmd.Env = append(os.Environ(),
		fmt.Sprintf("name=%s", jhelm.Name),
		fmt.Sprintf("namespace=%s", jhelm.Namespace),
		fmt.Sprintf("workspace=%s", workspace),
		fmt.Sprintf("chart=%s", jhelm.Spec.Source.Chart),
		fmt.Sprintf("gitsha=%s", sourceGitSha),
		fmt.Sprintf("checksum=%s", *checksum),
		fmt.Sprintf("app_version=%s", appVersion),
		fmt.Sprintf("value_files=%s", valueFiles),
		fmt.Sprintf("helm_secrets_key=%s", helmSecretsKey),
		fmt.Sprintf("helm_status_file=%s", fHelmStatus),
		fmt.Sprintf("helm_history_file=%s", fHelmHistory),
	)
	if _, err := cmd.Output(); err != nil {
		jhelm.NotReady("HelmUpgradeFailed", GetExitErrorStdErr(&err))
		r.UpdateStatus(ctx, jhelm)
		return r.Result(jhelm)
	}

	jhelm.Status.SpecSha = specShaSum
	SaveFile(fSpecSha, specShaSum)
	jhelm.Status.GitSha = sourceGitSha
	SaveFile(fGitSha, sourceGitSha)
	jhelm.Status.Checksum = *LoadFile(fChecksum)
	helmRevision = LoadHelmRevision(fHelmHistory)
	if helmRevision != nil {
		jhelm.Status.ChartVersion = helmRevision.Chart
		jhelm.Status.AppVersion = helmRevision.AppVersion
		jhelm.Status.Revision = helmRevision.Revision
		jhelm.Status.Updated = helmRevision.Updated
		jhelm.Status.Description = helmRevision.Description
	}
	jhelm.Ready("HelmUpgradeSucceeded")
	r.UpdateStatus(ctx, jhelm)
	return r.Result(jhelm)
}

// SetupWithManager sets up the controller with the Manager.
func (r *JHelmReleaseReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		WithOptions(controller.Options{MaxConcurrentReconciles: 3}).
		For(&cdv1.JHelmRelease{}).
		Complete(r)
}

func (r *JHelmReleaseReconciler) ObjectName(name string) types.NamespacedName {
	return types.NamespacedName{Name: name, Namespace: r.Options.Namespace}
}

func (r *JHelmReleaseReconciler) UpdateStatus(ctx context.Context, jhelm *cdv1.JHelmRelease) {
	log := logger.FromContext(ctx)
	if err := r.Status().Update(ctx, jhelm); err != nil {
		log.Info("Failed to update status", "error", err.Error())
	} else {
		s := &jhelm.Status
		log.Info(fmt.Sprintf("status=%s, reason=%s", s.Status, s.Reason))
	}
}

func (r *JHelmReleaseReconciler) Result(jhelm *cdv1.JHelmRelease) (ctrl.Result, error) {
	return ctrl.Result{RequeueAfter: jhelm.Spec.UpdateInterval.Duration}, nil
}

// todo unused; implement this
func (r *JHelmReleaseReconciler) CreateEvent(ctx context.Context, jhelm *cdv1.JHelmRelease) error {
	e := v1.Event{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "",
			Namespace: "",
		},
		TypeMeta: metav1.TypeMeta{
			Kind:       "",
			APIVersion: "",
		},
		InvolvedObject: v1.ObjectReference{
			Name:            "",
			Namespace:       "",
			Kind:            "",
			APIVersion:      "",
			UID:             "",
			ResourceVersion: "",
		},
		Action:  "",
		Reason:  "",
		Message: "",
		Source: v1.EventSource{
			Component: "",
			Host:      "",
		},
		//FirstTimestamp:      metav1.Time{},
		//LastTimestamp:       metav1.Time{Time: time.Now()},
		//Count:               0,
		//Type:                "",
		EventTime: metav1.MicroTime{Time: time.Now()},
		//Series:              nil,
		//Related:             nil,
		ReportingController: "",
		ReportingInstance:   "",
	}
	return r.Create(ctx, &e)
}

func LoadHelmRevision(fname string) *HelmRevision {
	all := []HelmRevision{}
	LoadObject(fname, &all)
	if len(all) > 0 {
		hh := all[len(all)-1]
		return &hh
	}
	return nil
}

type HelmStatus struct {
	Name      string `json:"name,omitempty"`
	Namespace string `json:"namespace,omitempty"`
	Version   int64  `json:"version,omitempty"`
	Info      struct {
		FirstDeployed string `json:"first_deployed,omitempty"`
		LastDeployed  string `json:"last_deployed,omitempty"`
		Deleted       string `json:"deleted,omitempty"`
		Description   string `json:"description,omitempty"`
		Status        string `json:"status,omitempty"`
	} `json:"info,omitempty"`
	Config   interface{} `json:"config,omitempty"`
	Manifest string      `json:"manifest,omitempty"`
}

type HelmRevision struct {
	Revision    int64  `json:"revision,omitempty"`
	Updated     string `json:"updated,omitempty"`
	Status      string `json:"status,omitempty"`
	Chart       string `json:"chart,omitempty"`
	AppVersion  string `json:"app_version,omitempty"`
	Description string `json:"description,omitempty"`
}

func (r *JHelmReleaseReconciler) LoadAppVersion(jhelm *cdv1.JHelmRelease) (*string, error) {
	workspace := path.Join(r.Options.Workspace, "jhelm", jhelm.Name)
	var file string
	var query string
	if jhelm.Spec.Package.SetAppVersion {
		file = path.Join(workspace, jhelm.Spec.Package.FromFile)
		query = jhelm.Spec.Package.ValuePath
	} else {
		file = path.Join(workspace, jhelm.Spec.Source.Chart, "Chart.yaml")
		query = "appVersion"
	}
	if yml, err := New(file); err == nil {
		x := yml.GetString(query)
		return &x, nil
	} else {
		return nil, err
	}
}
