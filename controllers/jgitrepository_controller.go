/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"git.jrevolt.io/jrevolt/cd/jgitops/api"
	cdv1 "git.jrevolt.io/jrevolt/cd/jgitops/api/v1"
	"io/ioutil"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	runtime "k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"os"
	"os/exec"
	"path"
	"reflect"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
	"strings"
	"time"
)

// JGitRepositoryReconciler reconciles a JGitRepository object
type JGitRepositoryReconciler struct {
	client.Client
	Scheme  *runtime.Scheme
	Options api.JGitopsOperatorOptions
}

// Reconcile
//+kubebuilder:rbac:groups=cd.jrevolt.io,resources=jgits,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=cd.jrevolt.io,resources=jgits/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=cd.jrevolt.io,resources=jgits/finalizers,verbs=update
func (r *JGitRepositoryReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.FromContext(ctx)

	//return ctrl.Result{}, nil

	jgit := &cdv1.JGitRepository{}
	if err := r.Get(ctx, req.NamespacedName, jgit); err != nil {
		if errors.IsNotFound(err) {
			log.Info("Deleted") // assuming all cleanup done in finalizer
			return ctrl.Result{}, nil
		} else {
			log.Error(err, "Failed to load", "req", req.NamespacedName)
			return ctrl.Result{}, err
		}
	}

	log.V(1).Info(fmt.Sprintf("generation=%d, version=%s", jgit.Generation, jgit.ResourceVersion))

	secret := &v1.Secret{}
	if err := r.Get(ctx, r.ObjectName(jgit.Spec.Secret.Ref), secret); err != nil {
		if errors.IsNotFound(err) {
			jgit.NotReady("Invalid", err.Error())
			r.Fail(ctx, jgit, err)
			return ctrl.Result{}, nil
		} else {
			return ctrl.Result{}, nil
		}
	}

	sshPrivateKey := secret.Data[jgit.Spec.Secret.SshPrivateKey]
	sshKnownHosts := secret.Data[jgit.Spec.Secret.SshKnownHosts]

	log = log.WithValues("generation", jgit.Generation, "resourceVersion", jgit.ResourceVersion)

	// register finalizer if needed
	finalizer := "jgitops"
	if !controllerutil.ContainsFinalizer(jgit, finalizer) {
		controllerutil.AddFinalizer(jgit, finalizer)
		if err := r.Update(ctx, jgit); err != nil {
			return ctrl.Result{}, nil
		}
	}

	workspace := path.Join(r.Options.Workspace, "jgit", jgit.Name)

	// finalization
	if jgit.GetDeletionTimestamp() != nil {
		if err := os.RemoveAll(workspace); err != nil {
			log.Error(err, "failed to cleanup workspace", "err", err.Error())
			return ctrl.Result{}, nil
		}
		controllerutil.RemoveFinalizer(jgit, finalizer)
		if err := r.Update(ctx, jgit); err != nil {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, nil
	}

	os.MkdirAll(workspace, os.ModeDir)

	outdated := false

	specsha := GetShaSum(jgit.Spec, secret.Data)
	fSpecSha := path.Join(workspace, ".specsha")
	wSpecSha := LoadFile(fSpecSha)
	isWorkspaceSynced := specsha == jgit.Status.SpecSha && wSpecSha != nil && *wSpecSha == specsha
	outdated = outdated || !isWorkspaceSynced

	isReconciliationRequested := false
	if x := jgit.GetReconciliationRequested(); x != nil {
		isReconciliationRequested = x.After(jgit.Status.LastReconciled.Time)
	}
	outdated = outdated || isReconciliationRequested

	isReconciliationScheduled := false
	if isWorkspaceSynced {
		// workspace is up to date, can this be a scheduled update?
		isReconciliationScheduled = jgit.Status.LastReconciled.Add(jgit.Spec.UpdateInterval.Duration).Before(time.Now())
	}
	outdated = outdated || isReconciliationScheduled

	revisions := r.LoadRevisions(workspace)
	isRevisionsUptodate := revisions != nil && reflect.DeepEqual(*revisions, jgit.Status.Revisions)
	outdated = outdated || !isRevisionsUptodate

	if jgit.IsReady() && !outdated {
		return r.Result(jgit)
	}

	//log.V(1).Info("Reconciling",
	//  "isWorkspaceSynced", isWorkspaceSynced,
	//  "isReconciliationRequested", isReconciliationRequested,
	//  "isReconciliationScheduled", isReconciliationScheduled)

	script := path.Join(r.Options.Scripts, "jgit.sh")

	cmdctx, cancel := context.WithTimeout(ctx, 60*time.Second)
	defer cancel()
	cmd := exec.CommandContext(cmdctx, r.Options.Bash, script, "update")
	cmd.Env = append(os.Environ(),
		fmt.Sprintf("name=%s", jgit.Name),
		fmt.Sprintf("namespace=%s", jgit.Namespace),
		fmt.Sprintf("generation=%d", jgit.Generation),
		fmt.Sprintf("workspace=%s", workspace),
		fmt.Sprintf("url=%s", jgit.Spec.Url),
		fmt.Sprintf("ssh_private_key=%s", sshPrivateKey),
		fmt.Sprintf("ssh_known_hosts=%s", sshKnownHosts),
	)
	if _, err := cmd.Output(); err != nil {
		jgit.NotReady("GitFetchFailed", GetExitErrorStdErr(&err))
		r.UpdateStatus(ctx, jgit)
		return r.Result(jgit)
	}

	SaveFile(fSpecSha, specsha)

	revisions = r.LoadRevisions(workspace)
	if revisions == nil {
		return ctrl.Result{RequeueAfter: 5 * time.Second}, nil
	}

	status := jgit.Status.DeepCopy()
	status.SpecSha = specsha
	status.Revisions = *revisions

	isHelmReconcileNeeded := !reflect.DeepEqual(*status, jgit.Status) || !jgit.IsReady()

	if isHelmReconcileNeeded {
		jgit.Status = *status
		jgit.Ready("GitFetchSucceeded")
		r.UpdateStatus(ctx, jgit)
		r.TriggerHelmReleaseReconcile(ctx, jgit)
	}

	return r.Result(jgit)
}

func (r *JGitRepositoryReconciler) TriggerHelmReleaseReconcile(ctx context.Context, jgit *cdv1.JGitRepository) {
	all := cdv1.JHelmReleaseList{}
	r.List(ctx, &all)
	// todo consider async https://hackernoon.com/asyncawait-in-golang-an-introductory-guide-ol1e34sg
	for _, jhelm := range all.Items {
		if jhelm.Spec.Source.Ref == jgit.Name {
			jhelm.Annotations["reconciliation-requested"] = time.Now().Format(time.RFC3339)
			r.Update(ctx, &jhelm)
		}
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *JGitRepositoryReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		WithOptions(controller.Options{MaxConcurrentReconciles: 3}).
		For(&cdv1.JGitRepository{}).
		Complete(r)
}

func (r *JGitRepositoryReconciler) UpdateStatus(ctx context.Context, jgit *cdv1.JGitRepository) {
	log := logger.FromContext(ctx)
	if err := r.Status().Update(ctx, jgit); err != nil {
		log.Info("Failed to update status", "error", err.Error())
	} else {
		s := &jgit.Status
		log.Info(fmt.Sprintf("status=%s, reason=%s", s.Status, s.Reason))
	}
}

func (r *JGitRepositoryReconciler) Result(jgit *cdv1.JGitRepository) (ctrl.Result, error) {
	return ctrl.Result{RequeueAfter: jgit.Spec.UpdateInterval.Duration}, nil
}

func (r *JGitRepositoryReconciler) ObjectName(name string) types.NamespacedName {
	return types.NamespacedName{Name: name, Namespace: r.Options.Namespace}
}

func (r *JGitRepositoryReconciler) Fail(ctx context.Context, jgit *cdv1.JGitRepository, err error) {
	log := logger.FromContext(ctx)
	log.Error(nil, "Failed", "err", err, "status", jgit.Status.Status, "reason", jgit.Status.Reason)
	if err := r.Status().Update(ctx, jgit); err != nil {
		log.Error(err, "Failed to update status")
	}
}

func (r *JGitRepositoryReconciler) LoadRevisions(workspace string) *map[string]string {
	dir := path.Join(workspace, "refs/remotes/origin")
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil
	}
	result := map[string]string{}
	for _, f := range files {
		read, err := os.ReadFile(path.Join(dir, f.Name()))
		if err != nil {
			return nil
		}
		branch := f.Name()
		gitsha := strings.TrimSpace(string(read))
		result[branch] = gitsha
	}
	return &result
}
