#syntax=docker/dockerfile:1.2

FROM library/alpine:3.12 as base

FROM base as curl
RUN apk add curl tar

# https://github.com/kubernetes/kubernetes/releases
FROM curl as download-kubectl
ARG KUBECTL_VERSION="v1.22.3"
RUN url="https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" &&\
    path="/usr/local/bin/kubectl" &&\
    curl "$url" -L -o "$path" && chmod +x "$path" &&\
    kubectl version --client

# https://github.com/helm/helm/releases
# https://get.helm.sh/helm-v3.6.3-linux-amd64.tar.gz
FROM curl as download-helm
ARG HELM_VERSION="v3.7.1"
RUN url="https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz" &&\
    curl -sL "$url" | tar xzv -C /usr/local/bin/ --strip-components=1 linux-amd64/helm &&\
    chown -R root:root /usr/local/bin/ &&\
    chmod -R a+r /usr/local/bin/ &&\
    helm version

FROM curl as download-kustomize
RUN ver=4.3.0 &&\
    url=https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${ver}/kustomize_v${ver}_linux_amd64.tar.gz &&\
    curl -sL $url | tar xzv -C /usr/local/bin/ && chmod a+x /usr/local/bin


FROM base as kubetools
#ENV PATH="/root/.krew/bin:$PATH"
RUN --mount=type=cache,target=/mnt \
    --mount=from=download-kubectl,source=/usr/local/bin,target=/mnt/kubectl/usr/local/bin \
    --mount=from=download-helm,source=/usr/local/bin,target=/mnt/helm/usr/local/bin \
    --mount=from=download-kustomize,source=/usr/local/bin,target=/mnt/kustomize/usr/local/bin \
    echo "Installing core libraries..." &&\
    apk add --no-cache bash curl tar coreutils moreutils openssl git openssh-client &&\
    echo "Installing utilities (kubectl/helm/gitversion)..." &&\
    tar c \
      -C /mnt/kubectl/ . \
      -C /mnt/helm/ . \
      -C /mnt/kustomize/ . \
      | tar xv -C /


FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as dotnet6

FROM base as yamlsecrets
RUN --mount=type=cache,target=/var/cache \
    apk add bash curl icu-libs tzdata
ARG REPO_URL
ARG REPO_USER
ARG REPO_PASS
ENV PATH="/usr/share/dotnet:$PATH"
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false
RUN --mount=type=cache,target=/root \
    --mount=from=dotnet6,source=/usr/share/dotnet,target=/usr/share/dotnet \
    dotnet tool install JRevolt.YamlSecrets.Cli --version="1.*" --tool-path=/opt/tools


FROM kubetools as jgitops-base
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT="false"
ENV DOTNET_CLI_TELEMETRY_OPTOUT="true"
ENV PATH="/opt/tools:/usr/share/dotnet:$PATH"
RUN --mount=type=cache,target=/var/cache \
    --mount=type=cache,target=/tmp \
    apk add bash nodejs &&\
    apk add rsync icu-libs tzdata jq vim &&\
    apk add py3-pip && pip install yq
RUN --mount=type=cache,target=/mnt \
    --mount=from=mcr.microsoft.com/dotnet/aspnet:6.0-alpine,src=/usr/share/dotnet,dst=/mnt/fs/usr/share/dotnet \
    --mount=from=yamlsecrets,src=/opt/tools,dst=/mnt/fs/opt/tools \
    rsync -r /mnt/fs/ /

FROM jgitops-base as jgitops
ENV JGITOPS_SCRIPTS="/opt/jgitops/scripts"
ENV JGITOPS_PLUGINS="/opt/jgitops/helm-plugins"
RUN --mount=type=cache,target=/mnt \
    --mount=target=/mnt/context \
    basedir=/opt/jgitops &&\
    mkdir -p ${basedir} &&\
    rsync -r /mnt/context/config/crd/bases/ ${basedir}/crd/ &&\
    rsync -r /mnt/context/scripts/ "$JGITOPS_SCRIPTS" &&\
    rsync -r /mnt/context/helm-plugins/ "$JGITOPS_PLUGINS" &&\
    rsync -r /mnt/context/charts/ ${basedir}/charts/ &&\
    install -D -m 555 /mnt/context/jgitops ${basedir}/bin/jgitops &&\
    ln -s ${basedir}/bin/jgitops /usr/local/bin/jgitops &&\
    echo "DONE"
