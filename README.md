# JGitOps

Opinionated Gitops Operator

Highlights:
- use `JGitRepository` CRD to configure connection to source Git repository with Helm-based deployment
  configurations
- use `JHelmRelease` CRD to configure a Helm release deployment, referencing a directory in a
  `JGitRepository` as a source
- *JGitOps* operator monitors configured Git repositories for changes and triggers Helm upgrade
  when changes are detected

Special Features:
- Both `git-update` and Helm `helm-upgrade` operations are implemented as customizable shell
  scripts
- *JGitOps* operator is only an orchestrator/coordinator
- Supports Helm values encryption using `YamlSecrets` as YAML encryption tool, and custom
  Helm plugin implementation

## JGitRepository

Simplified example:
```yaml
apiVersion: cd.jrevolt.io/v1
kind: JGitRepository
metadata:
  name: mygit
spec:
  url: git@example.com:path/to/repo.git
  secret:
    ref: mygit-secret
    sshKnownHosts: known_hosts
    sshPrivateKey: id_rsa
```

## JHelmRelease

Simplified example:
```yaml
apiVersion: cd.jrevolt.io/v1
kind: JHelmRelease
metadata:
  name: nginx
spec:
  source:
    ref: mygit
    branch: main
    chart: charts/nginx
  secrets:
    key: secrets.key
    ref: mygit-secrets
  valueFiles:
  - secrets://charts/nginx/secrets.yaml
```

## Helm Secrets

### installation

```shell
# YamlSecrets CLI
dotnet tool install jrevolt.yamlsecrets.cli --version "1.*" -g

# Helm Plugin
target=/opt/jgitops/helm-plugins
curl https://gitlab.com/jrevolt/cd/jgitops/-/archive/main/jgitops-main.tar.gz |
tar xzv -C $target jgitops-main/helm-plugins --strip-components 2
export HELM_PLUGINS=$target
```

### usage

Generate key

```shell
yaml-secrets keygen -i secrets.key
```

Edit your values:
```yaml
username: johndoe
password: !<secret> topsecret
```

Encrypt your values:
```shell
helm secrets enc --key=secrets.pub --file=values.yaml
```

See result:
```yaml
username: johndoe
password: !<encrypted> "gAAA...9zUA=="
```

Keep `secrets.pub` in your repository.

Protect `secrets.key`. If you keep your secrets write-only, you only need private key during helm deployment.

Helm deployment:
```shell
helm upgrade --install my-release ./my-chart -f secrets://my-chart/values.yaml
```






