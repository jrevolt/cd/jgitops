#!/bin/bash
set -eu

fail() {
  echo "[ERR] $@" >&2
  exit 1
}

# simple CLI parser
# usage:
# source <(args "$@")
# source <(args "--key1=value1" "--key2=value2")
args() {
  for i in "$@"; do
    echo -n "$i" | perl -E '
    $in = join("", <STDIN>);
    $k=$in; $k =~ s/^(?:--)?([^=]+)=.*$/$1/gms;
    $v=$in; $v =~ s/^[^=]+=(.*)$/$1/gms; $v =~ s/"/\\"/gms;
    say "local $k=\"$v\""'
  done
}

#which cygpath &>/dev/null &&
#realpath() {
#  cygpath -wam "$1"
#}

downloader() {
  local file=${4//*:\/\//}
  local key=${YAMLSECRETS_KEY:-${HELM_SECRETS_KEY:-secrets.key}}
  [ -f ${file} ] || fail "Missing value file: ${4} => $(realpath $file)"
  [ -f ${key} ] || fail "Missing decryption key: \$YAMLSECRETS_KEY => $key => $(realpath $key)"
  yaml-secrets dec -y -k "$key" -f "$file"
}

enc() {
  local key
  local file
  source <(args "$@")
  [[ $key ]]
  [[ $file ]]
  [[ -f $key ]] && local keyfile=$key
  echo "Encrypting ${file} using key ${keyfile:-inline}"
  yaml-secrets enc -y -k "$key" -f "$file" -o "$file"
}

dec() {
  local key
  local file
  source <(args "$@")
  [[ $key ]]
  [[ $file ]]
  [[ -f $key ]] && local keyfile=$key
  echo "Decrypting ${file} using key ${keyfile:-inline}"
  yaml-secrets dec -y -k "$key" -f "$file" -o "$file"
}


"$@"

