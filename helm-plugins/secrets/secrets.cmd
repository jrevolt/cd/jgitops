@echo off
setlocal enableextensions enabledelayedexpansion
FOR /F "tokens=* USEBACKQ" %%F IN (`cygpath -a %HELM_PLUGIN_DIR%\secrets.sh`) DO (
  set script=%%F
)
if "%BASHEXE%"=="" set BASHEXE=bash
%BASHEXE% %script%  %*%
exit /b %errorlevel%

