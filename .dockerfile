FROM alpine:3.15
RUN apk add bash curl git go make
RUN export ARCH=$(case $(uname -m) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(uname -m) ;; esac) ;\
    export OS=$(uname | awk '{print tolower($0)}') ;\
    export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/v1.15.0 ;\
    curl -sv -L ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH} | install -m 755 /dev/stdin /usr/local/bin/operator-sdk
ENTRYPOINT ["/usr/local/bin/operator-sdk"]
